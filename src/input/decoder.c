/*****************************************************************************
 * decoder.c: Functions for the management of decoders
 *****************************************************************************
 * Copyright (C) 1999-2004 the VideoLAN team
 * $Id$
 *
 * Authors: Christophe Massiot <massiot@via.ecp.fr>
 *          Gildas Bazin <gbazin@videolan.org>
 *          Laurent Aimar <fenrir@via.ecp.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <assert.h>

#include <vlc_common.h>

#include <vlc_block.h>
#include <vlc_vout.h>
#include <vlc_aout.h>
#include <vlc_sout.h>
#include <vlc_codec.h>
#include <vlc_osd.h>

#include <vlc_interface.h>
#include "audio_output/aout_internal.h"
#include "stream_output/stream_output.h"
#include "input_internal.h"
#include "clock.h"
#include "decoder.h"

#include "../video_output/vout_internal.h"

static decoder_t *CreateDecoder( input_thread_t *, es_format_t *, int, sout_instance_t *p_sout );
static void       DeleteDecoder( decoder_t * );

static void      *DecoderThread( vlc_object_t * );
static int        DecoderProcess( decoder_t *, block_t * );
static void       DecoderOutputChangePause( decoder_t *, bool b_paused, mtime_t i_date );
static void       DecoderFlush( decoder_t * );
static void       DecoderSignalBuffering( decoder_t *, bool );
static void       DecoderFlushBuffering( decoder_t * );

static void       DecoderUnsupportedCodec( decoder_t *, vlc_fourcc_t );

/* Buffers allocation callbacks for the decoders */
static aout_buffer_t *aout_new_buffer( decoder_t *, int );
static void aout_del_buffer( decoder_t *, aout_buffer_t * );

static picture_t *vout_new_buffer( decoder_t * );
static void vout_del_buffer( decoder_t *, picture_t * );
static void vout_link_picture( decoder_t *, picture_t * );
static void vout_unlink_picture( decoder_t *, picture_t * );

static subpicture_t *spu_new_buffer( decoder_t * );
static void spu_del_buffer( decoder_t *, subpicture_t * );

static es_format_t null_es_format;

struct decoder_owner_sys_t
{
    int64_t         i_preroll_end;

    input_thread_t  *p_input;
    input_clock_t   *p_clock;
    int             i_last_rate;

    vout_thread_t   *p_spu_vout;
    int              i_spu_channel;
    int64_t          i_spu_order;

    sout_instance_t         *p_sout;
    sout_packetizer_input_t *p_sout_input;

    /* Some decoders require already packetized data (ie. not truncated) */
    decoder_t *p_packetizer;

    /* Current format in use by the output */
    video_format_t video;
    audio_format_t audio;
    es_format_t    sout;

    /* fifo */
    block_fifo_t *p_fifo;

    /* Lock for communication with decoder thread */
    vlc_mutex_t lock;
    vlc_cond_t  wait;

    /* -- These variables need locking on write(only) -- */
    aout_instance_t *p_aout;
    aout_input_t    *p_aout_input;

    vout_thread_t   *p_vout;

    /* -- Theses variables need locking on read *and* write -- */
    /* */
    /* Pause */
    bool b_paused;
    struct
    {
        mtime_t i_date;
    } pause;

    /* Buffering */
    bool b_buffering;
    struct
    {
        bool b_first;
        bool b_full;
        int  i_count;

        picture_t     *p_picture;
        picture_t     **pp_picture_next;

        subpicture_t  *p_subpic;
        subpicture_t  **pp_subpic_next;

        aout_buffer_t *p_audio;
        aout_buffer_t **pp_audio_next;
    } buffer;

    /* Flushing */
    bool b_flushing;

    /* CC */
    struct
    {
        bool b_supported;
        bool pb_present[4];
        decoder_t *pp_decoder[4];
    } cc;

    /* Delay */
    mtime_t i_ts_delay;
};

#define DECODER_MAX_BUFFERING_COUNT (4)
#define DECODER_MAX_BUFFERING_AUDIO_DURATION (AOUT_MAX_PREPARE_TIME)
#define DECODER_MAX_BUFFERING_VIDEO_DURATION (1*CLOCK_FREQ)

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/* decoder_GetInputAttachment:
 */
input_attachment_t *decoder_GetInputAttachment( decoder_t *p_dec,
                                                const char *psz_name )
{
    input_attachment_t *p_attachment;
    if( input_Control( p_dec->p_owner->p_input, INPUT_GET_ATTACHMENT, &p_attachment, psz_name ) )
        return NULL;
    return p_attachment;
}
/* decoder_GetInputAttachments:
 */
int decoder_GetInputAttachments( decoder_t *p_dec,
                                 input_attachment_t ***ppp_attachment,
                                 int *pi_attachment )
{
    return input_Control( p_dec->p_owner->p_input, INPUT_GET_ATTACHMENTS,
                          ppp_attachment, pi_attachment );
}
/* decoder_GetDisplayDate:
 */
mtime_t decoder_GetDisplayDate( decoder_t *p_dec, mtime_t i_ts )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );
    if( p_owner->b_buffering )
        i_ts = 0;
    vlc_mutex_unlock( &p_owner->lock );

    if( !p_owner->p_clock || !i_ts )
        return i_ts;

    return input_clock_GetTS( p_owner->p_clock, NULL, p_owner->p_input->i_pts_delay, i_ts );
}
/* decoder_GetDisplayRate:
 */
int decoder_GetDisplayRate( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    if( !p_owner->p_clock )
        return INPUT_RATE_DEFAULT;
    return input_clock_GetRate( p_owner->p_clock );
}

/**
 * Spawns a new decoder thread
 *
 * \param p_input the input thread
 * \param p_es the es descriptor
 * \return the spawned decoder object
 */
decoder_t *input_DecoderNew( input_thread_t *p_input,
                             es_format_t *fmt, input_clock_t *p_clock, sout_instance_t *p_sout  )
{
    decoder_t *p_dec = NULL;
    int i_priority;

#ifdef ENABLE_SOUT
    /* If we are in sout mode, search for packetizer module */
    if( p_sout )
    {
        /* Create the decoder configuration structure */
        p_dec = CreateDecoder( p_input, fmt, VLC_OBJECT_PACKETIZER, p_sout );
        if( p_dec == NULL )
        {
            msg_Err( p_input, "could not create packetizer" );
            intf_UserFatal( p_input, false, _("Streaming / Transcoding failed"),
                            _("VLC could not open the packetizer module.") );
            return NULL;
        }
    }
    else
#endif
    {
        /* Create the decoder configuration structure */
        p_dec = CreateDecoder( p_input, fmt, VLC_OBJECT_DECODER, p_sout );
        if( p_dec == NULL )
        {
            msg_Err( p_input, "could not create decoder" );
            intf_UserFatal( p_input, false, _("Streaming / Transcoding failed"),
                            _("VLC could not open the decoder module.") );
            return NULL;
        }
    }

    if( !p_dec->p_module )
    {
        DecoderUnsupportedCodec( p_dec, fmt->i_codec );

        DeleteDecoder( p_dec );
        vlc_object_release( p_dec );
        return NULL;
    }

    p_dec->p_owner->p_clock = p_clock;

    if( fmt->i_cat == AUDIO_ES )
        i_priority = VLC_THREAD_PRIORITY_AUDIO;
    else
        i_priority = VLC_THREAD_PRIORITY_VIDEO;

    /* Spawn the decoder thread */
    if( vlc_thread_create( p_dec, "decoder", DecoderThread,
                           i_priority, false ) )
    {
        msg_Err( p_dec, "cannot spawn decoder thread" );
        module_unneed( p_dec, p_dec->p_module );
        DeleteDecoder( p_dec );
        vlc_object_release( p_dec );
        return NULL;
    }

    return p_dec;
}

/**
 * Kills a decoder thread and waits until it's finished
 *
 * \param p_input the input thread
 * \param p_es the es descriptor
 * \return nothing
 */
void input_DecoderDelete( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_object_kill( p_dec );

    /* Make sure we aren't paused anymore */
    vlc_mutex_lock( &p_owner->lock );
    if( p_owner->b_paused || p_owner->b_buffering )
    {
        p_owner->b_paused = false;
        p_owner->b_buffering = false;
        vlc_cond_signal( &p_owner->wait );
    }
    vlc_mutex_unlock( &p_owner->lock );

    /* Make sure the thread leaves the function */
    block_FifoWake( p_owner->p_fifo );

    vlc_thread_join( p_dec );

    /* Don't module_unneed() here because of the dll loader that wants
     * close() in the same thread than open()/decode() */

    /* */
    if( p_dec->p_owner->cc.b_supported )
    {
        int i;
        for( i = 0; i < 4; i++ )
            input_DecoderSetCcState( p_dec, false, i );
    }

    /* Delete decoder configuration */
    DeleteDecoder( p_dec );

    /* Delete the decoder */
    vlc_object_release( p_dec );
}

/**
 * Put a block_t in the decoder's fifo.
 *
 * \param p_dec the decoder object
 * \param p_block the data block
 */
void input_DecoderDecode( decoder_t *p_dec, block_t *p_block )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    if( p_owner->p_input->p->b_out_pace_control )
    {
        /* FIXME !!!!! */
        while( vlc_object_alive( p_dec ) && !p_dec->b_error &&
               block_FifoCount( p_owner->p_fifo ) > 10 )
        {
            msleep( 1000 );
        }
    }
    else if( block_FifoSize( p_owner->p_fifo ) > 50000000 /* 50 MB */ )
    {
        /* FIXME: ideally we would check the time amount of data
         * in the fifo instead of its size. */
        msg_Warn( p_dec, "decoder/packetizer fifo full (data not "
                  "consumed quickly enough), resetting fifo!" );
        block_FifoEmpty( p_owner->p_fifo );
    }

    block_FifoPut( p_owner->p_fifo, p_block );
}

bool input_DecoderIsEmpty( decoder_t * p_dec )
{
    assert( !p_dec->p_owner->b_buffering );

    /* FIXME that's not really true */
    return block_FifoCount( p_dec->p_owner->p_fifo ) <= 0;
}

void input_DecoderIsCcPresent( decoder_t *p_dec, bool pb_present[4] )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    int i;

    vlc_mutex_lock( &p_owner->lock );
    for( i = 0; i < 4; i++ )
        pb_present[i] =  p_owner->cc.pb_present[i];
    vlc_mutex_unlock( &p_owner->lock );
}
int input_DecoderSetCcState( decoder_t *p_dec, bool b_decode, int i_channel )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    //msg_Warn( p_dec, "input_DecoderSetCcState: %d @%d", b_decode, i_channel );

    if( i_channel < 0 || i_channel >= 4 || !p_owner->cc.pb_present[i_channel] )
        return VLC_EGENERIC;

    if( b_decode )
    {
        static const vlc_fourcc_t fcc[4] = {
            VLC_FOURCC('c', 'c', '1', ' '),
            VLC_FOURCC('c', 'c', '2', ' '),
            VLC_FOURCC('c', 'c', '3', ' '),
            VLC_FOURCC('c', 'c', '4', ' '),
        };
        decoder_t *p_cc;
        es_format_t fmt;

        es_format_Init( &fmt, SPU_ES, fcc[i_channel] );
        p_cc = CreateDecoder( p_owner->p_input, &fmt, VLC_OBJECT_DECODER, p_owner->p_sout );
        if( !p_cc )
        {
            msg_Err( p_dec, "could not create decoder" );
            intf_UserFatal( p_dec, false, _("Streaming / Transcoding failed"),
                            _("VLC could not open the decoder module.") );
            return VLC_EGENERIC;
        }
        else if( !p_cc->p_module )
        {
            DecoderUnsupportedCodec( p_dec, fcc[i_channel] );
            DeleteDecoder( p_cc );
            vlc_object_release( p_cc );
            return VLC_EGENERIC;
        }
        p_cc->p_owner->p_clock = p_owner->p_clock;

        vlc_mutex_lock( &p_owner->lock );
        p_owner->cc.pp_decoder[i_channel] = p_cc;
        vlc_mutex_unlock( &p_owner->lock );
    }
    else
    {
        decoder_t *p_cc;

        vlc_mutex_lock( &p_owner->lock );
        p_cc = p_owner->cc.pp_decoder[i_channel];
        p_owner->cc.pp_decoder[i_channel] = NULL;
        vlc_mutex_unlock( &p_owner->lock );

        if( p_cc )
        {
            vlc_object_kill( p_cc );
            module_unneed( p_cc, p_cc->p_module );
            DeleteDecoder( p_cc );
            vlc_object_release( p_cc );
        }
    }
    return VLC_SUCCESS;
}
int input_DecoderGetCcState( decoder_t *p_dec, bool *pb_decode, int i_channel )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    *pb_decode = false;
    if( i_channel < 0 || i_channel >= 4 || !p_owner->cc.pb_present[i_channel] )
        return VLC_EGENERIC;

    vlc_mutex_lock( &p_owner->lock );
    *pb_decode = p_owner->cc.pp_decoder[i_channel] != NULL;
    vlc_mutex_unlock( &p_owner->lock );
    return VLC_EGENERIC;
}

void input_DecoderChangePause( decoder_t *p_dec, bool b_paused, mtime_t i_date )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );

    assert( !p_owner->b_paused || !b_paused );

    p_owner->b_paused = b_paused;
    p_owner->pause.i_date = i_date;
    vlc_cond_signal( &p_owner->wait );

    DecoderOutputChangePause( p_dec, b_paused, i_date );

    vlc_mutex_unlock( &p_owner->lock );
}

void input_DecoderChangeDelay( decoder_t *p_dec, mtime_t i_delay )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );
    p_owner->i_ts_delay = i_delay;
    vlc_mutex_unlock( &p_owner->lock );
}

void input_DecoderStartBuffering( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );

    DecoderFlush( p_dec );

    p_owner->buffer.b_first = true;
    p_owner->buffer.b_full = false;
    p_owner->buffer.i_count = 0;

    assert( !p_owner->buffer.p_picture && !p_owner->buffer.p_subpic && !p_owner->buffer.p_audio );

    p_owner->buffer.p_picture = NULL;
    p_owner->buffer.pp_picture_next = &p_owner->buffer.p_picture;

    p_owner->buffer.p_subpic = NULL;
    p_owner->buffer.pp_subpic_next = &p_owner->buffer.p_subpic;

    p_owner->buffer.p_audio = NULL;
    p_owner->buffer.pp_audio_next = &p_owner->buffer.p_audio;

    p_owner->b_buffering = true;

    vlc_cond_signal( &p_owner->wait );

    vlc_mutex_unlock( &p_owner->lock );
}

void input_DecoderStopBuffering( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );

    p_owner->b_buffering = false;

    vlc_cond_signal( &p_owner->wait );

    vlc_mutex_unlock( &p_owner->lock );
}

void input_DecoderWaitBuffering( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );

    while( vlc_object_alive( p_dec ) && p_owner->b_buffering && !p_owner->buffer.b_full )
    {
        block_FifoWake( p_owner->p_fifo );
        vlc_cond_wait( &p_owner->wait, &p_owner->lock );
    }

    vlc_mutex_unlock( &p_owner->lock );
}

/*****************************************************************************
 * Internal functions
 *****************************************************************************/

/* */
static void DecoderUnsupportedCodec( decoder_t *p_dec, vlc_fourcc_t codec )
{
    msg_Err( p_dec, "no suitable decoder module for fourcc `%4.4s'.\n"
             "VLC probably does not support this sound or video format.",
             (char*)&codec );
    intf_UserFatal( p_dec, false, _("No suitable decoder module"), 
                    _("VLC does not support the audio or video format \"%4.4s\". "
                      "Unfortunately there is no way for you to fix this."), (char*)&codec );
}


/**
 * Create a decoder object
 *
 * \param p_input the input thread
 * \param p_es the es descriptor
 * \param i_object_type Object type as define in include/vlc_objects.h
 * \return the decoder object
 */
static decoder_t * CreateDecoder( input_thread_t *p_input,
                                  es_format_t *fmt, int i_object_type, sout_instance_t *p_sout )
{
    decoder_t *p_dec;
    decoder_owner_sys_t *p_owner;
    int i;

    p_dec = vlc_object_create( p_input, i_object_type );
    if( p_dec == NULL )
        return NULL;

    p_dec->pf_decode_audio = NULL;
    p_dec->pf_decode_video = NULL;
    p_dec->pf_decode_sub = NULL;
    p_dec->pf_get_cc = NULL;
    p_dec->pf_packetize = NULL;

    /* Initialize the decoder fifo */
    p_dec->p_module = NULL;

    memset( &null_es_format, 0, sizeof(es_format_t) );
    es_format_Copy( &p_dec->fmt_in, fmt );
    es_format_Copy( &p_dec->fmt_out, &null_es_format );

    /* Allocate our private structure for the decoder */
    p_dec->p_owner = p_owner = malloc( sizeof( decoder_owner_sys_t ) );
    if( p_dec->p_owner == NULL )
    {
        vlc_object_release( p_dec );
        return NULL;
    }
    p_dec->p_owner->i_preroll_end = -1;
    p_dec->p_owner->i_last_rate = INPUT_RATE_DEFAULT;
    p_dec->p_owner->p_input = p_input;
    p_dec->p_owner->p_aout = NULL;
    p_dec->p_owner->p_aout_input = NULL;
    p_dec->p_owner->p_vout = NULL;
    p_dec->p_owner->p_spu_vout = NULL;
    p_dec->p_owner->i_spu_channel = 0;
    p_dec->p_owner->i_spu_order = 0;
    p_dec->p_owner->p_sout = p_sout;
    p_dec->p_owner->p_sout_input = NULL;
    p_dec->p_owner->p_packetizer = NULL;

    /* decoder fifo */
    if( ( p_dec->p_owner->p_fifo = block_FifoNew() ) == NULL )
    {
        free( p_dec->p_owner );
        vlc_object_release( p_dec );
        return NULL;
    }

    /* Set buffers allocation callbacks for the decoders */
    p_dec->pf_aout_buffer_new = aout_new_buffer;
    p_dec->pf_aout_buffer_del = aout_del_buffer;
    p_dec->pf_vout_buffer_new = vout_new_buffer;
    p_dec->pf_vout_buffer_del = vout_del_buffer;
    p_dec->pf_picture_link    = vout_link_picture;
    p_dec->pf_picture_unlink  = vout_unlink_picture;
    p_dec->pf_spu_buffer_new  = spu_new_buffer;
    p_dec->pf_spu_buffer_del  = spu_del_buffer;

    vlc_object_attach( p_dec, p_input );

    /* Find a suitable decoder/packetizer module */
    if( i_object_type == VLC_OBJECT_DECODER )
        p_dec->p_module = module_need( p_dec, "decoder", "$codec", 0 );
    else
        p_dec->p_module = module_need( p_dec, "packetizer", "$packetizer", 0 );

    /* Check if decoder requires already packetized data */
    if( i_object_type == VLC_OBJECT_DECODER &&
        p_dec->b_need_packetized && !p_dec->fmt_in.b_packetized )
    {
        p_dec->p_owner->p_packetizer =
            vlc_object_create( p_input, VLC_OBJECT_PACKETIZER );
        if( p_dec->p_owner->p_packetizer )
        {
            es_format_Copy( &p_dec->p_owner->p_packetizer->fmt_in,
                            &p_dec->fmt_in );

            es_format_Copy( &p_dec->p_owner->p_packetizer->fmt_out,
                            &null_es_format );

            vlc_object_attach( p_dec->p_owner->p_packetizer, p_input );

            p_dec->p_owner->p_packetizer->p_module =
                module_need( p_dec->p_owner->p_packetizer,
                             "packetizer", "$packetizer", 0 );

            if( !p_dec->p_owner->p_packetizer->p_module )
            {
                es_format_Clean( &p_dec->p_owner->p_packetizer->fmt_in );
                vlc_object_detach( p_dec->p_owner->p_packetizer );
                vlc_object_release( p_dec->p_owner->p_packetizer );
            }
        }
    }

    /* Copy ourself the input replay gain */
    if( fmt->i_cat == AUDIO_ES )
    {
        for( i = 0; i < AUDIO_REPLAY_GAIN_MAX; i++ )
        {
            if( !p_dec->fmt_out.audio_replay_gain.pb_peak[i] )
            {
                p_dec->fmt_out.audio_replay_gain.pb_peak[i] = fmt->audio_replay_gain.pb_peak[i];
                p_dec->fmt_out.audio_replay_gain.pf_peak[i] = fmt->audio_replay_gain.pf_peak[i];
            }
            if( !p_dec->fmt_out.audio_replay_gain.pb_gain[i] )
            {
                p_dec->fmt_out.audio_replay_gain.pb_gain[i] = fmt->audio_replay_gain.pb_gain[i];
                p_dec->fmt_out.audio_replay_gain.pf_gain[i] = fmt->audio_replay_gain.pf_gain[i];
            }
        }
    }

    /* */
    vlc_mutex_init( &p_owner->lock );
    vlc_cond_init( &p_owner->wait );

    p_owner->b_paused = false;
    p_owner->pause.i_date = 0;

    p_owner->b_buffering = false;
    p_owner->buffer.b_first = true;
    p_owner->buffer.b_full = false;
    p_owner->buffer.i_count = 0;
    p_owner->buffer.p_picture = NULL;
    p_owner->buffer.p_subpic = NULL;
    p_owner->buffer.p_audio = NULL;

    p_owner->b_flushing = false;

    /* */
    p_owner->cc.b_supported = false;
    if( i_object_type == VLC_OBJECT_DECODER )
    {
        if( p_owner->p_packetizer && p_owner->p_packetizer->pf_get_cc )
            p_owner->cc.b_supported = true;
        if( p_dec->pf_get_cc )
            p_owner->cc.b_supported = true;
    }

    for( i = 0; i < 4; i++ )
    {
        p_owner->cc.pb_present[i] = false;
        p_owner->cc.pp_decoder[i] = NULL;
    }
    p_owner->i_ts_delay = 0;
    return p_dec;
}

/**
 * The decoding main loop
 *
 * \param p_dec the decoder
 */
static void *DecoderThread( vlc_object_t *p_this )
{
    decoder_t *p_dec = (decoder_t *)p_this;
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    int canc = vlc_savecancel();

    /* The decoder's main loop */
    while( vlc_object_alive( p_dec ) && !p_dec->b_error )
    {
        block_t *p_block = block_FifoGet( p_owner->p_fifo );

        DecoderSignalBuffering( p_dec, p_block == NULL );

        if( p_block && DecoderProcess( p_dec, p_block ) != VLC_SUCCESS )
            break;
    }

    while( vlc_object_alive( p_dec ) )
    {
        block_t *p_block = block_FifoGet( p_owner->p_fifo );

        DecoderSignalBuffering( p_dec, p_block == NULL );

        /* Trash all received PES packets */
        if( p_block )
            block_Release( p_block );
    }
    DecoderSignalBuffering( p_dec, true );

    /* We do it here because of the dll loader that wants close() in the
     * same thread than open()/decode() */
    module_unneed( p_dec, p_dec->p_module );
    vlc_restorecancel( canc );
    return NULL;
}

static void DecoderFlush( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    block_t *p_null;

    vlc_assert_locked( &p_owner->lock );

    /* Empty the fifo */
    block_FifoEmpty( p_owner->p_fifo );

    /* Monitor for flush end */
    p_owner->b_flushing = true;
    vlc_cond_signal( &p_owner->wait );

    /* Send a special block */
    p_null = block_New( p_dec, 128 );
    if( !p_null )
        return;
    p_null->i_flags |= BLOCK_FLAG_DISCONTINUITY;
    p_null->i_flags |= BLOCK_FLAG_CORE_FLUSH;
    if( !p_dec->fmt_in.b_packetized )
        p_null->i_flags |= BLOCK_FLAG_CORRUPTED;
    memset( p_null->p_buffer, 0, p_null->i_buffer );

    input_DecoderDecode( p_dec, p_null );

    /* */
    while( vlc_object_alive( p_dec ) && p_owner->b_flushing )
        vlc_cond_wait( &p_owner->wait, &p_owner->lock );
}

static void DecoderSignalFlushed( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );

    if( p_owner->b_flushing )
    {
        p_owner->b_flushing = false;
        vlc_cond_signal( &p_owner->wait );
    }

    vlc_mutex_unlock( &p_owner->lock );
}

static void DecoderSignalBuffering( decoder_t *p_dec, bool b_full )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_mutex_lock( &p_owner->lock );

    if( p_owner->b_buffering )
    {
        if( b_full )
            p_owner->buffer.b_full = true;
        vlc_cond_signal( &p_owner->wait );
    }

    vlc_mutex_unlock( &p_owner->lock );
}

static bool DecoderIsFlushing( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    bool b_flushing;

    vlc_mutex_lock( &p_owner->lock );

    b_flushing = p_owner->b_flushing;

    vlc_mutex_unlock( &p_owner->lock );

    return b_flushing;
}

static void DecoderWaitUnblock( decoder_t *p_dec, bool *pb_reject )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_assert_locked( &p_owner->lock );

    while( !p_owner->b_flushing )
    {
        if( p_owner->b_paused && p_owner->b_buffering && !p_owner->buffer.b_full )
            break;

        if( !p_owner->b_paused && ( !p_owner->b_buffering || !p_owner->buffer.b_full ) )
            break;

        vlc_cond_wait( &p_owner->wait, &p_owner->lock );
    }

    if( pb_reject )
        *pb_reject = p_owner->b_flushing;
}

static void DecoderOutputChangePause( decoder_t *p_dec, bool b_paused, mtime_t i_date )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_assert_locked( &p_owner->lock );

    /* XXX only audio and video output have to be paused.
     * - for sout it is useless
     * - for subs, it is done by the vout
     */
    if( p_dec->fmt_in.i_cat == AUDIO_ES )
    {
        if( p_owner->p_aout && p_owner->p_aout_input )
            aout_DecChangePause( p_owner->p_aout, p_owner->p_aout_input,
                                 b_paused, i_date );
    }
    else if( p_dec->fmt_in.i_cat == VIDEO_ES )
    {
        if( p_owner->p_vout )
            vout_ChangePause( p_owner->p_vout, b_paused, i_date );
    }
}
static inline void DecoderUpdatePreroll( int64_t *pi_preroll, const block_t *p )
{
    if( p->i_flags & (BLOCK_FLAG_PREROLL|BLOCK_FLAG_DISCONTINUITY) )
        *pi_preroll = INT64_MAX;
    else if( p->i_pts > 0 )
        *pi_preroll = __MIN( *pi_preroll, p->i_pts );
    else if( p->i_dts > 0 )
        *pi_preroll = __MIN( *pi_preroll, p->i_dts );
}

static mtime_t DecoderTeletextFixTs( mtime_t i_ts, mtime_t i_ts_delay )
{
    mtime_t current_date = mdate();

    /* FIXME I don't really like that, es_out SHOULD do it using the video pts */
    if( !i_ts || i_ts > current_date + 10000000 || i_ts < current_date )
    {
        /* ETSI EN 300 472 Annex A : do not take into account the PTS
         * for teletext streams. */
        return current_date + 400000 + i_ts_delay;
    }
    return i_ts;
}

static void DecoderFixTs( decoder_t *p_dec, mtime_t *pi_ts0, mtime_t *pi_ts1,
                          mtime_t *pi_duration, int *pi_rate, mtime_t *pi_delay, bool b_telx )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    input_clock_t   *p_clock = p_owner->p_clock;
    int i_rate = 0;

    vlc_assert_locked( &p_owner->lock );

    const mtime_t i_ts_delay = p_owner->p_input->i_pts_delay;
    const mtime_t i_es_delay = p_owner->i_ts_delay;

    if( p_clock )
    {
        const bool b_ephemere = pi_ts1 && *pi_ts0 == *pi_ts1;

        if( *pi_ts0 > 0 )
            *pi_ts0 = input_clock_GetTS( p_clock, &i_rate, i_ts_delay,
                                         *pi_ts0 + i_es_delay );
        if( pi_ts1 && *pi_ts1 > 0 )
            *pi_ts1 = input_clock_GetTS( p_clock, &i_rate, i_ts_delay,
                                         *pi_ts1 + i_es_delay );

        /* Do not create ephemere data because of rounding errors */
        if( !b_ephemere && pi_ts1 && *pi_ts0 == *pi_ts1 )
            *pi_ts1 += 1;

        if( i_rate <= 0 )
            i_rate = input_clock_GetRate( p_clock ); 

        if( pi_duration )
            *pi_duration = ( *pi_duration * i_rate +
                                    INPUT_RATE_DEFAULT-1 ) / INPUT_RATE_DEFAULT;

        if( pi_rate )
            *pi_rate = i_rate;

        if( b_telx )
        {
            *pi_ts0 = DecoderTeletextFixTs( *pi_ts0, i_ts_delay );
            if( pi_ts1 && *pi_ts1 <= 0 )
                *pi_ts1 = *pi_ts0;
        }
    }
    if( pi_delay )
    {
        const int r = i_rate > 0 ? i_rate : INPUT_RATE_DEFAULT;
        *pi_delay = i_ts_delay + i_es_delay * r / INPUT_RATE_DEFAULT;
    }
}

static void DecoderPlayAudio( decoder_t *p_dec, aout_buffer_t *p_audio,
                              int *pi_played_sum, int *pi_lost_sum )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    aout_instance_t *p_aout = p_owner->p_aout;
    aout_input_t    *p_aout_input = p_owner->p_aout_input;

    /* */
    if( p_audio->start_date <= 0 )
    {
        msg_Warn( p_dec, "non-dated audio buffer received" );
        *pi_lost_sum += 1;
        aout_BufferFree( p_audio );
        return;
    }

    /* */
    vlc_mutex_lock( &p_owner->lock );

    if( p_owner->b_buffering || p_owner->buffer.p_audio )
    {
        p_audio->p_next = NULL;

        *p_owner->buffer.pp_audio_next = p_audio;
        p_owner->buffer.pp_audio_next = &p_audio->p_next;

        p_owner->buffer.i_count++;
        if( p_owner->buffer.i_count > DECODER_MAX_BUFFERING_COUNT ||
            p_audio->start_date - p_owner->buffer.p_audio->start_date > DECODER_MAX_BUFFERING_AUDIO_DURATION )
        {
            p_owner->buffer.b_full = true;
            vlc_cond_signal( &p_owner->wait );
        }
    }

    for( ;; )
    {
        bool b_has_more = false;
        bool b_reject;
        DecoderWaitUnblock( p_dec, &b_reject );

        if( p_owner->b_buffering )
        {
            vlc_mutex_unlock( &p_owner->lock );
            return;
        }

        /* */
        if( p_owner->buffer.p_audio )
        {
            p_audio = p_owner->buffer.p_audio;

            p_owner->buffer.p_audio = p_audio->p_next;
            p_owner->buffer.i_count--;

            b_has_more = p_owner->buffer.p_audio != NULL;
            if( !b_has_more )
                p_owner->buffer.pp_audio_next = &p_owner->buffer.p_audio;
        }

        /* */
        int i_rate = INPUT_RATE_DEFAULT;
        mtime_t i_delay;

        DecoderFixTs( p_dec, &p_audio->start_date, &p_audio->end_date, NULL,
                      &i_rate, &i_delay, false );

        vlc_mutex_unlock( &p_owner->lock );

        /* */
        const mtime_t i_max_date = mdate() + i_delay + AOUT_MAX_ADVANCE_TIME;

        if( !p_aout || !p_aout_input ||
            p_audio->start_date <= 0 || p_audio->start_date > i_max_date ||
            i_rate < INPUT_RATE_DEFAULT/AOUT_MAX_INPUT_RATE ||
            i_rate > INPUT_RATE_DEFAULT*AOUT_MAX_INPUT_RATE )
            b_reject = true;

        if( !b_reject )
        {
            /* Wait if we are too early
             * FIXME that's plain ugly to do it here */
            mwait( p_audio->start_date - AOUT_MAX_PREPARE_TIME );

            if( !aout_DecPlay( p_aout, p_aout_input, p_audio, i_rate ) )
                *pi_played_sum += 1;
            *pi_lost_sum += aout_DecGetResetLost( p_aout, p_aout_input );
        }
        else
        {
            if( p_audio->start_date <= 0 )
            {
                msg_Warn( p_dec, "non-dated audio buffer received" );
            }
            else if( p_audio->start_date > i_max_date )
            {
                msg_Warn( p_aout, "received buffer in the future (%"PRId64")",
                          p_audio->start_date - mdate() );
            }
            *pi_lost_sum += 1;
            aout_BufferFree( p_audio );
        }

        if( !b_has_more )
            break;

        vlc_mutex_lock( &p_owner->lock );
        if( !p_owner->buffer.p_audio )
        {
            vlc_mutex_unlock( &p_owner->lock );
            break;
        }
    }
}

static void DecoderDecodeAudio( decoder_t *p_dec, block_t *p_block )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    input_thread_t  *p_input = p_owner->p_input;
    aout_buffer_t   *p_aout_buf;
    int i_decoded = 0;
    int i_lost = 0;
    int i_played = 0;

    while( (p_aout_buf = p_dec->pf_decode_audio( p_dec, &p_block )) )
    {
        aout_instance_t *p_aout = p_owner->p_aout;
        aout_input_t    *p_aout_input = p_owner->p_aout_input;
        int i_lost = 0;
        int i_played;

        if( p_dec->b_die )
        {
            /* It prevent freezing VLC in case of broken decoder */
            aout_DecDeleteBuffer( p_aout, p_aout_input, p_aout_buf );
            if( p_block )
                block_Release( p_block );
            break;
        }
        i_decoded++;

        if( p_aout_buf->start_date < p_owner->i_preroll_end )
        {
            aout_DecDeleteBuffer( p_aout, p_aout_input, p_aout_buf );
            continue;
        }

        if( p_owner->i_preroll_end > 0 )
        {
            msg_Dbg( p_dec, "End of audio preroll" );
            if( p_owner->p_aout && p_owner->p_aout_input )
                aout_DecFlush( p_owner->p_aout, p_owner->p_aout_input );
            /* */
            p_owner->i_preroll_end = -1;
        }

        DecoderPlayAudio( p_dec, p_aout_buf, &i_played, &i_lost );
    }

    /* Update ugly stat */
    if( i_decoded > 0 || i_lost > 0 || i_played > 0 )
    {
        vlc_mutex_lock( &p_input->p->counters.counters_lock);

        stats_UpdateInteger( p_dec, p_input->p->counters.p_lost_abuffers,
                             i_lost, NULL );
        stats_UpdateInteger( p_dec, p_input->p->counters.p_played_abuffers,
                             i_played, NULL );
        stats_UpdateInteger( p_dec, p_input->p->counters.p_decoded_audio,
                             i_decoded, NULL );

        vlc_mutex_unlock( &p_input->p->counters.counters_lock);
    }
}
static void DecoderGetCc( decoder_t *p_dec, decoder_t *p_dec_cc )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    block_t *p_cc;
    bool pb_present[4];
    int i;
    int i_cc_decoder;

    assert( p_dec_cc->pf_get_cc != NULL );

    /* Do not try retreiving CC if not wanted (sout) or cannot be retreived */
    if( !p_owner->cc.b_supported )
        return;

    p_cc = p_dec_cc->pf_get_cc( p_dec_cc, pb_present );
    if( !p_cc )
        return;

    vlc_mutex_lock( &p_owner->lock );
    for( i = 0, i_cc_decoder = 0; i < 4; i++ )
    {
        p_owner->cc.pb_present[i] |= pb_present[i];
        if( p_owner->cc.pp_decoder[i] )
            i_cc_decoder++;
    }

    for( i = 0; i < 4; i++ )
    {
        if( !p_owner->cc.pp_decoder[i] )
            continue;

        if( i_cc_decoder > 1 )
            DecoderProcess( p_owner->cc.pp_decoder[i], block_Duplicate( p_cc ) );
        else
            DecoderProcess( p_owner->cc.pp_decoder[i], p_cc );
        i_cc_decoder--;
    }
    vlc_mutex_unlock( &p_owner->lock );
}

static void DecoderPlayVideo( decoder_t *p_dec, picture_t *p_picture,
                              int *pi_played_sum, int *pi_lost_sum )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    vout_thread_t  *p_vout = p_owner->p_vout;

    if( p_picture->date <= 0 )
    {
        msg_Warn( p_vout, "non-dated video buffer received" );
        *pi_lost_sum += 1;
        vout_DropPicture( p_vout, p_picture );
        return;
    }

    /* */
    vlc_mutex_lock( &p_owner->lock );

    if( ( p_owner->b_buffering && !p_owner->buffer.b_first ) || p_owner->buffer.p_picture )
    {
        p_picture->p_next = NULL;

        *p_owner->buffer.pp_picture_next = p_picture;
        p_owner->buffer.pp_picture_next = &p_picture->p_next;

        p_owner->buffer.i_count++;
        if( p_owner->buffer.i_count > DECODER_MAX_BUFFERING_COUNT ||
            p_picture->date - p_owner->buffer.p_picture->date > DECODER_MAX_BUFFERING_VIDEO_DURATION )
        {
            p_owner->buffer.b_full = true;
            vlc_cond_signal( &p_owner->wait );
        }
    }

    for( ;; )
    {
        bool b_has_more = false;

        bool b_reject;
        DecoderWaitUnblock( p_dec, &b_reject );

        if( p_owner->b_buffering && !p_owner->buffer.b_first )
        {
            vlc_mutex_unlock( &p_owner->lock );
            return;
        }
        bool b_buffering_first = p_owner->b_buffering;

        /* */
        if( p_owner->buffer.p_picture )
        {
            p_picture = p_owner->buffer.p_picture;

            p_owner->buffer.p_picture = p_picture->p_next;
            p_owner->buffer.i_count--;

            b_has_more = p_owner->buffer.p_picture != NULL;
            if( !b_has_more )
                p_owner->buffer.pp_picture_next = &p_owner->buffer.p_picture;
        }

        /* */
        int i_rate = INPUT_RATE_DEFAULT;
        mtime_t i_delay;

        if( b_buffering_first )
        {
            assert( p_owner->buffer.b_first );
            assert( !p_owner->buffer.i_count );
            msg_Dbg( p_dec, "Received first picture" );
            p_owner->buffer.b_first = false;
            p_picture->date = mdate();
            p_picture->b_force = true;
            i_delay = 0;
            if( p_owner->p_clock )
                i_rate = input_clock_GetRate( p_owner->p_clock );
        }
        else
        {
            DecoderFixTs( p_dec, &p_picture->date, NULL, NULL,
                          &i_rate, &i_delay, false );
        }

        vlc_mutex_unlock( &p_owner->lock );

        /* */
        const mtime_t i_max_date = mdate() + i_delay + VOUT_BOGUS_DELAY;

        if( p_picture->date <= 0 || p_picture->date >= i_max_date )
            b_reject = true;

        if( !b_reject )
        {
            if( i_rate != p_owner->i_last_rate  )
            {
                /* Be sure to not display old picture after our own */
                vout_Flush( p_vout, p_picture->date );
                p_owner->i_last_rate = i_rate;
            }

            vout_DisplayPicture( p_vout, p_picture );
        }
        else
        {
            if( p_picture->date <= 0 )
            {
                msg_Warn( p_vout, "non-dated video buffer received" );
            }
            else
            {
                msg_Warn( p_vout, "early picture skipped (%"PRId64")",
                          p_picture->date - mdate() );
            }
            *pi_lost_sum += 1;
            vout_DropPicture( p_vout, p_picture );
        }
        int i_tmp_display;
        int i_tmp_lost;
        vout_GetResetStatistic( p_vout, &i_tmp_display, &i_tmp_lost );

        *pi_played_sum += i_tmp_display;
        *pi_lost_sum += i_tmp_lost;

        if( !b_has_more || b_buffering_first )
            break;

        vlc_mutex_lock( &p_owner->lock );
        if( !p_owner->buffer.p_picture )
        {
            vlc_mutex_unlock( &p_owner->lock );
            break;
        }
    }
}

static void DecoderDecodeVideo( decoder_t *p_dec, block_t *p_block )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    input_thread_t *p_input = p_owner->p_input;
    picture_t      *p_pic;
    int i_lost = 0;
    int i_decoded = 0;
    int i_displayed = 0;

    while( (p_pic = p_dec->pf_decode_video( p_dec, &p_block )) )
    {
        vout_thread_t  *p_vout = p_owner->p_vout;
        if( p_dec->b_die )
        {
            /* It prevent freezing VLC in case of broken decoder */
            vout_DropPicture( p_vout, p_pic );
            if( p_block )
                block_Release( p_block );
            break;
        }

        i_decoded++;

        if( p_pic->date < p_owner->i_preroll_end )
        {
            vout_DropPicture( p_vout, p_pic );
            continue;
        }

        if( p_owner->i_preroll_end > 0 )
        {
            msg_Dbg( p_dec, "End of video preroll" );
            if( p_vout )
                vout_Flush( p_vout, 1 );
            /* */
            p_owner->i_preroll_end = -1;
        }

        if( p_dec->pf_get_cc &&
            ( !p_owner->p_packetizer || !p_owner->p_packetizer->pf_get_cc ) )
            DecoderGetCc( p_dec, p_dec );

        DecoderPlayVideo( p_dec, p_pic, &i_displayed, &i_lost );
    }
    if( i_decoded > 0 || i_lost > 0 || i_displayed > 0 )
    {
        vlc_mutex_lock( &p_input->p->counters.counters_lock );

        stats_UpdateInteger( p_dec, p_input->p->counters.p_decoded_video,
                             i_decoded, NULL );
        stats_UpdateInteger( p_dec, p_input->p->counters.p_lost_pictures,
                             i_lost , NULL);

        stats_UpdateInteger( p_dec, p_input->p->counters.p_displayed_pictures,
                             i_displayed, NULL);

        vlc_mutex_unlock( &p_input->p->counters.counters_lock );
    }
}

static void DecoderPlaySpu( decoder_t *p_dec, subpicture_t *p_subpic,
                            bool b_telx )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    vout_thread_t *p_vout = p_owner->p_spu_vout;

    /* */
    if( p_subpic->i_start <= 0 )
    {
        msg_Warn( p_dec, "non-dated spu buffer received" );
        subpicture_Delete( p_subpic );
        return;
    }

    /* */
    vlc_mutex_lock( &p_owner->lock );

    if( p_owner->b_buffering || p_owner->buffer.p_subpic )
    {
        p_subpic->p_next = NULL;

        *p_owner->buffer.pp_subpic_next = p_subpic;
        p_owner->buffer.pp_subpic_next = &p_subpic->p_next;

        p_owner->buffer.i_count++;
        /* XXX it is important to be full after the first one */
        if( p_owner->buffer.i_count > 0 )
        {
            p_owner->buffer.b_full = true;
            vlc_cond_signal( &p_owner->wait );
        }
    }

    for( ;; )
    {
        bool b_has_more = false;
        bool b_reject;
        DecoderWaitUnblock( p_dec, &b_reject );

        if( p_owner->b_buffering )
        {
            vlc_mutex_unlock( &p_owner->lock );
            return;
        }

        /* */
        if( p_owner->buffer.p_subpic )
        {
            p_subpic = p_owner->buffer.p_subpic;

            p_owner->buffer.p_subpic = p_subpic->p_next;
            p_owner->buffer.i_count--;

            b_has_more = p_owner->buffer.p_subpic != NULL;
            if( !b_has_more )
                p_owner->buffer.pp_subpic_next = &p_owner->buffer.p_subpic;
        }

        /* */
        DecoderFixTs( p_dec, &p_subpic->i_start, &p_subpic->i_stop, NULL,
                      NULL, NULL, b_telx );

        vlc_mutex_unlock( &p_owner->lock );

        if( !b_reject )
            spu_DisplaySubpicture( p_vout->p_spu, p_subpic );
        else
            subpicture_Delete( p_subpic );

        if( !b_has_more )
            break;
        vlc_mutex_lock( &p_owner->lock );
        if( !p_owner->buffer.p_subpic )
        {
            vlc_mutex_unlock( &p_owner->lock );
            break;
        }
    }
}

static void DecoderPlaySout( decoder_t *p_dec, block_t *p_sout_block,
                             bool b_telx )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    assert( p_owner->p_clock );

    vlc_mutex_lock( &p_owner->lock );

    bool b_reject;
    DecoderWaitUnblock( p_dec, &b_reject );

    DecoderFixTs( p_dec, &p_sout_block->i_dts, &p_sout_block->i_pts, &p_sout_block->i_length,
                  &p_sout_block->i_rate, NULL, b_telx );

    vlc_mutex_unlock( &p_owner->lock );

    sout_InputSendBuffer( p_owner->p_sout_input, p_sout_block );
}

/* */
static void DecoderFlushBuffering( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    vlc_assert_locked( &p_owner->lock );

    while( p_owner->buffer.p_picture )
    {
        picture_t *p_picture = p_owner->buffer.p_picture;

        p_owner->buffer.p_picture = p_picture->p_next;
        p_owner->buffer.i_count--;

        if( p_owner->p_vout )
            vout_DropPicture( p_owner->p_vout, p_picture );

        if( !p_owner->buffer.p_picture )
            p_owner->buffer.pp_picture_next = &p_owner->buffer.p_picture;
    }
    while( p_owner->buffer.p_audio )
    {
        aout_buffer_t *p_audio = p_owner->buffer.p_audio;

        p_owner->buffer.p_audio = p_audio->p_next;
        p_owner->buffer.i_count--;

        aout_BufferFree( p_audio );

        if( !p_owner->buffer.p_audio )
            p_owner->buffer.pp_audio_next = &p_owner->buffer.p_audio;
    }
    while( p_owner->buffer.p_subpic )
    {
        subpicture_t *p_subpic = p_owner->buffer.p_subpic;

        p_owner->buffer.p_subpic = p_subpic->p_next;
        p_owner->buffer.i_count--;

        subpicture_Delete( p_subpic );

        if( !p_owner->buffer.p_subpic )
            p_owner->buffer.pp_subpic_next = &p_owner->buffer.p_subpic;
    }
}

/* This function process a block for sout
 */
static void DecoderProcessSout( decoder_t *p_dec, block_t *p_block )
{
    decoder_owner_sys_t *p_owner = (decoder_owner_sys_t *)p_dec->p_owner;
    const bool b_telx = p_dec->fmt_in.i_codec == VLC_FOURCC('t','e','l','x');
    block_t *p_sout_block;

    while( ( p_sout_block =
                 p_dec->pf_packetize( p_dec, p_block ? &p_block : NULL ) ) )
    {
        if( !p_owner->p_sout_input )
        {
            es_format_Copy( &p_owner->sout, &p_dec->fmt_out );

            p_owner->sout.i_group = p_dec->fmt_in.i_group;
            p_owner->sout.i_id = p_dec->fmt_in.i_id;
            if( p_dec->fmt_in.psz_language )
            {
                if( p_owner->sout.psz_language )
                    free( p_owner->sout.psz_language );
                p_owner->sout.psz_language =
                    strdup( p_dec->fmt_in.psz_language );
            }

            p_owner->p_sout_input =
                sout_InputNew( p_owner->p_sout,
                               &p_owner->sout );

            if( p_owner->p_sout_input == NULL )
            {
                msg_Err( p_dec, "cannot create packetizer output (%4.4s)",
                         (char *)&p_owner->sout.i_codec );
                p_dec->b_error = true;

                while( p_sout_block )
                {
                    block_t *p_next = p_sout_block->p_next;
                    block_Release( p_sout_block );
                    p_sout_block = p_next;
                }
                break;
            }
        }

        while( p_sout_block )
        {
            block_t *p_next = p_sout_block->p_next;

            p_sout_block->p_next = NULL;

            DecoderPlaySout( p_dec, p_sout_block, b_telx );

            p_sout_block = p_next;
        }

        /* For now it's enough, as only sout impact on this flag */
        if( p_owner->p_sout->i_out_pace_nocontrol > 0 &&
            p_owner->p_input->p->b_out_pace_control )
        {
            msg_Dbg( p_dec, "switching to sync mode" );
            p_owner->p_input->p->b_out_pace_control = false;
        }
        else if( p_owner->p_sout->i_out_pace_nocontrol <= 0 &&
                 !p_owner->p_input->p->b_out_pace_control )
        {
            msg_Dbg( p_dec, "switching to async mode" );
            p_owner->p_input->p->b_out_pace_control = true;
        }
    }
}

/* This function process a video block
 */
static void DecoderProcessVideo( decoder_t *p_dec, block_t *p_block, bool b_flush )
{
    decoder_owner_sys_t *p_owner = (decoder_owner_sys_t *)p_dec->p_owner;

    if( p_owner->p_packetizer )
    {
        block_t *p_packetized_block;
        decoder_t *p_packetizer = p_owner->p_packetizer;

        while( (p_packetized_block =
                p_packetizer->pf_packetize( p_packetizer, p_block ? &p_block : NULL )) )
        {
            if( p_packetizer->fmt_out.i_extra && !p_dec->fmt_in.i_extra )
            {
                es_format_Clean( &p_dec->fmt_in );
                es_format_Copy( &p_dec->fmt_in, &p_packetizer->fmt_out );
            }
            if( p_packetizer->pf_get_cc )
                DecoderGetCc( p_dec, p_packetizer );

            while( p_packetized_block )
            {
                block_t *p_next = p_packetized_block->p_next;
                p_packetized_block->p_next = NULL;

                DecoderDecodeVideo( p_dec, p_packetized_block );

                p_packetized_block = p_next;
            }
        }
    }
    else if( p_block )
    {
        DecoderDecodeVideo( p_dec, p_block );
    }

    if( b_flush && p_owner->p_vout )
        vout_Flush( p_owner->p_vout, 1 );
}

/* This function process a audio block
 */
static void DecoderProcessAudio( decoder_t *p_dec, block_t *p_block, bool b_flush )
{
    decoder_owner_sys_t *p_owner = (decoder_owner_sys_t *)p_dec->p_owner;

    if( p_owner->p_packetizer )
    {
        block_t *p_packetized_block;
        decoder_t *p_packetizer = p_owner->p_packetizer;

        while( (p_packetized_block =
                p_packetizer->pf_packetize( p_packetizer, p_block ? &p_block : NULL )) )
        {
            if( p_packetizer->fmt_out.i_extra && !p_dec->fmt_in.i_extra )
            {
                es_format_Clean( &p_dec->fmt_in );
                es_format_Copy( &p_dec->fmt_in, &p_packetizer->fmt_out );
            }

            while( p_packetized_block )
            {
                block_t *p_next = p_packetized_block->p_next;
                p_packetized_block->p_next = NULL;

                DecoderDecodeAudio( p_dec, p_packetized_block );

                p_packetized_block = p_next;
            }
        }
    }
    else if( p_block )
    {
        DecoderDecodeAudio( p_dec, p_block );
    }

    if( b_flush && p_owner->p_aout && p_owner->p_aout_input )
        aout_DecFlush( p_owner->p_aout, p_owner->p_aout_input );
}

/* This function process a subtitle block
 */
static void DecoderProcessSpu( decoder_t *p_dec, block_t *p_block, bool b_flush )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    const bool b_telx = p_dec->fmt_in.i_codec == VLC_FOURCC('t','e','l','x');

    input_thread_t *p_input = p_owner->p_input;
    vout_thread_t *p_vout;
    subpicture_t *p_spu;

    while( (p_spu = p_dec->pf_decode_sub( p_dec, p_block ? &p_block : NULL ) ) )
    {
        vlc_mutex_lock( &p_input->p->counters.counters_lock );
        stats_UpdateInteger( p_dec, p_input->p->counters.p_decoded_sub, 1, NULL );
        vlc_mutex_unlock( &p_input->p->counters.counters_lock );

        p_vout = vlc_object_find( p_dec, VLC_OBJECT_VOUT, FIND_ANYWHERE );
        if( p_vout && p_owner->p_spu_vout == p_vout )
        {
            /* Preroll does not work very well with subtitle */
            if( p_spu->i_start > 0 &&
                p_spu->i_start < p_owner->i_preroll_end &&
                ( p_spu->i_stop <= 0 || p_spu->i_stop < p_owner->i_preroll_end ) )
            {
                subpicture_Delete( p_spu );
            }
            else
            {
                DecoderPlaySpu( p_dec, p_spu, b_telx );
            }
        }
        else
        {
            subpicture_Delete( p_spu );
        }
        if( p_vout )
            vlc_object_release( p_vout );
    }

    if( b_flush && p_owner->p_spu_vout )
    {
        p_vout = vlc_object_find( p_dec, VLC_OBJECT_VOUT, FIND_ANYWHERE );

        if( p_vout && p_owner->p_spu_vout == p_vout )
            spu_Control( p_vout->p_spu, SPU_CHANNEL_CLEAR,
                         p_owner->i_spu_channel );

        if( p_vout )
            vlc_object_release( p_vout );
    }
}

/**
 * Decode a block
 *
 * \param p_dec the decoder object
 * \param p_block the block to decode
 * \return VLC_SUCCESS or an error code
 */
static int DecoderProcess( decoder_t *p_dec, block_t *p_block )
{
    decoder_owner_sys_t *p_owner = (decoder_owner_sys_t *)p_dec->p_owner;
    const bool b_flush_request = p_block && (p_block->i_flags & BLOCK_FLAG_CORE_FLUSH);

    if( p_block && p_block->i_buffer <= 0 )
    {
        assert( !b_flush_request );
        block_Release( p_block );
        return VLC_SUCCESS;
    }

#ifdef ENABLE_SOUT
    if( p_dec->i_object_type == VLC_OBJECT_PACKETIZER )
    {
        if( p_block )
            p_block->i_flags &= ~BLOCK_FLAG_CORE_PRIVATE_MASK;

        DecoderProcessSout( p_dec, p_block );
    }
    else
#endif
    {
        bool b_flush = false;

        if( p_block )
        {
            const bool b_flushing = p_owner->i_preroll_end == INT64_MAX;
            DecoderUpdatePreroll( &p_owner->i_preroll_end, p_block );

            b_flush = !b_flushing && b_flush_request;

            p_block->i_flags &= ~BLOCK_FLAG_CORE_PRIVATE_MASK;
        }

        if( p_dec->fmt_in.i_cat == AUDIO_ES )
        {
            DecoderProcessAudio( p_dec, p_block, b_flush );
        }
        else if( p_dec->fmt_in.i_cat == VIDEO_ES )
        {
            DecoderProcessVideo( p_dec, p_block, b_flush );
        }
        else if( p_dec->fmt_in.i_cat == SPU_ES )
        {
            DecoderProcessSpu( p_dec, p_block, b_flush );
        }
        else
        {
            msg_Err( p_dec, "unknown ES format" );
            p_dec->b_error = true;
        }
    }

    /* */
    if( b_flush_request )
    {
        vlc_mutex_lock( &p_owner->lock );
        DecoderFlushBuffering( p_dec );
        vlc_mutex_unlock( &p_owner->lock );

        DecoderSignalFlushed( p_dec );
    }

    return p_dec->b_error ? VLC_EGENERIC : VLC_SUCCESS;
}

/**
 * Destroys a decoder object
 *
 * \param p_dec the decoder object
 * \return nothing
 */
static void DeleteDecoder( decoder_t * p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    msg_Dbg( p_dec, "killing decoder fourcc `%4.4s', %u PES in FIFO",
             (char*)&p_dec->fmt_in.i_codec,
             (unsigned)block_FifoCount( p_owner->p_fifo ) );

    /* Free all packets still in the decoder fifo. */
    block_FifoEmpty( p_owner->p_fifo );
    block_FifoRelease( p_owner->p_fifo );

    /* */
    vlc_mutex_lock( &p_owner->lock );
    DecoderFlushBuffering( p_dec );
    vlc_mutex_unlock( &p_owner->lock );

    /* Cleanup */
    if( p_owner->p_aout_input )
        aout_DecDelete( p_owner->p_aout, p_owner->p_aout_input );
    if( p_owner->p_aout )
    {
        vlc_object_release( p_owner->p_aout );
        p_owner->p_aout = NULL;
    }
    if( p_owner->p_vout )
    {
        /* Hack to make sure all the the pictures are freed by the decoder */
        vout_FixLeaks( p_owner->p_vout, true );

        /* We are about to die. Reattach video output to p_vlc. */
        vout_Request( p_dec, p_owner->p_vout, NULL );
        var_SetBool( p_owner->p_input, "intf-change-vout", true );
    }

#ifdef ENABLE_SOUT
    if( p_owner->p_sout_input )
    {
        sout_InputDelete( p_owner->p_sout_input );
        es_format_Clean( &p_owner->sout );
    }
#endif

    if( p_dec->fmt_in.i_cat == SPU_ES )
    {
        vout_thread_t *p_vout;

        p_vout = vlc_object_find( p_dec, VLC_OBJECT_VOUT, FIND_ANYWHERE );
        if( p_vout && p_owner->p_spu_vout == p_vout )
        {
            spu_Control( p_vout->p_spu, SPU_CHANNEL_CLEAR,
                         p_owner->i_spu_channel );
            vlc_object_release( p_vout );
        }
    }

    es_format_Clean( &p_dec->fmt_in );
    es_format_Clean( &p_dec->fmt_out );

    if( p_owner->p_packetizer )
    {
        module_unneed( p_owner->p_packetizer,
                       p_owner->p_packetizer->p_module );
        es_format_Clean( &p_owner->p_packetizer->fmt_in );
        es_format_Clean( &p_owner->p_packetizer->fmt_out );
        vlc_object_detach( p_owner->p_packetizer );
        vlc_object_release( p_owner->p_packetizer );
    }

    vlc_cond_destroy( &p_owner->wait );
    vlc_mutex_destroy( &p_owner->lock );

    vlc_object_detach( p_dec );

    free( p_owner );
}

/*****************************************************************************
 * Buffers allocation callbacks for the decoders
 *****************************************************************************/
static aout_buffer_t *aout_new_buffer( decoder_t *p_dec, int i_samples )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    aout_buffer_t *p_buffer;

    if( p_owner->p_aout_input != NULL &&
        ( p_dec->fmt_out.audio.i_rate != p_owner->audio.i_rate ||
          p_dec->fmt_out.audio.i_original_channels !=
              p_owner->audio.i_original_channels ||
          p_dec->fmt_out.audio.i_bytes_per_frame !=
              p_owner->audio.i_bytes_per_frame ) )
    {
        aout_input_t *p_aout_input = p_owner->p_aout_input;

        /* Parameters changed, restart the aout */
        vlc_mutex_lock( &p_owner->lock );

        DecoderFlushBuffering( p_dec );

        p_owner->p_aout_input = NULL;
        aout_DecDelete( p_owner->p_aout, p_aout_input );

        vlc_mutex_unlock( &p_owner->lock );
    }

    if( p_owner->p_aout_input == NULL )
    {
        const int i_force_dolby = config_GetInt( p_dec, "force-dolby-surround" );
        audio_sample_format_t format;
        aout_input_t *p_aout_input;
        aout_instance_t *p_aout;

        p_dec->fmt_out.audio.i_format = p_dec->fmt_out.i_codec;
        p_owner->audio = p_dec->fmt_out.audio;

        memcpy( &format, &p_owner->audio, sizeof( audio_sample_format_t ) );
        if ( i_force_dolby && (format.i_original_channels&AOUT_CHAN_PHYSMASK)
                                    == (AOUT_CHAN_LEFT|AOUT_CHAN_RIGHT) )
        {
            if ( i_force_dolby == 1 )
            {
                format.i_original_channels = format.i_original_channels |
                                             AOUT_CHAN_DOLBYSTEREO;
            }
            else /* i_force_dolby == 2 */
            {
                format.i_original_channels = format.i_original_channels &
                                             ~AOUT_CHAN_DOLBYSTEREO;
            }
        }

        p_aout = p_owner->p_aout;
        p_aout_input = aout_DecNew( p_dec, &p_aout,
                                    &format, &p_dec->fmt_out.audio_replay_gain );

        vlc_mutex_lock( &p_owner->lock );
        p_owner->p_aout = p_aout;
        p_owner->p_aout_input = p_aout_input;
        vlc_mutex_unlock( &p_owner->lock );

        if( p_owner->p_aout_input == NULL )
        {
            msg_Err( p_dec, "failed to create audio output" );
            p_dec->b_error = true;
            return NULL;
        }
        p_dec->fmt_out.audio.i_bytes_per_frame =
            p_owner->audio.i_bytes_per_frame;
    }

    p_buffer = aout_DecNewBuffer( p_owner->p_aout_input, i_samples );

    return p_buffer;
}

static void aout_del_buffer( decoder_t *p_dec, aout_buffer_t *p_buffer )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    aout_DecDeleteBuffer( p_owner->p_aout,
                          p_owner->p_aout_input, p_buffer );
}


int vout_CountPictureAvailable( vout_thread_t *p_vout );

static picture_t *vout_new_buffer( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;

    if( p_owner->p_vout == NULL ||
        p_dec->fmt_out.video.i_width != p_owner->video.i_width ||
        p_dec->fmt_out.video.i_height != p_owner->video.i_height ||
        p_dec->fmt_out.video.i_chroma != p_owner->video.i_chroma ||
        p_dec->fmt_out.video.i_aspect != p_owner->video.i_aspect )
    {
        vout_thread_t *p_vout;

        if( !p_dec->fmt_out.video.i_width ||
            !p_dec->fmt_out.video.i_height )
        {
            /* Can't create a new vout without display size */
            return NULL;
        }

        if( !p_dec->fmt_out.video.i_visible_width ||
            !p_dec->fmt_out.video.i_visible_height )
        {
            if( p_dec->fmt_in.video.i_visible_width &&
                p_dec->fmt_in.video.i_visible_height )
            {
                p_dec->fmt_out.video.i_visible_width =
                    p_dec->fmt_in.video.i_visible_width;
                p_dec->fmt_out.video.i_visible_height =
                    p_dec->fmt_in.video.i_visible_height;
            }
            else
            {
                p_dec->fmt_out.video.i_visible_width =
                    p_dec->fmt_out.video.i_width;
                p_dec->fmt_out.video.i_visible_height =
                    p_dec->fmt_out.video.i_height;
            }
        }

        if( p_dec->fmt_out.video.i_visible_height == 1088 &&
            var_CreateGetBool( p_dec, "hdtv-fix" ) )
        {
            p_dec->fmt_out.video.i_visible_height = 1080;
            p_dec->fmt_out.video.i_sar_num *= 135;
            p_dec->fmt_out.video.i_sar_den *= 136;
            msg_Warn( p_dec, "Fixing broken HDTV stream (display_height=1088)");
        }

        if( !p_dec->fmt_out.video.i_sar_num ||
            !p_dec->fmt_out.video.i_sar_den )
        {
            p_dec->fmt_out.video.i_sar_num = p_dec->fmt_out.video.i_aspect *
              p_dec->fmt_out.video.i_visible_height;

            p_dec->fmt_out.video.i_sar_den = VOUT_ASPECT_FACTOR *
              p_dec->fmt_out.video.i_visible_width;
        }

        vlc_ureduce( &p_dec->fmt_out.video.i_sar_num,
                     &p_dec->fmt_out.video.i_sar_den,
                     p_dec->fmt_out.video.i_sar_num,
                     p_dec->fmt_out.video.i_sar_den, 50000 );

        p_dec->fmt_out.video.i_chroma = p_dec->fmt_out.i_codec;
        p_owner->video = p_dec->fmt_out.video;

        vlc_mutex_lock( &p_owner->lock );

        DecoderFlushBuffering( p_dec );

        p_vout = p_owner->p_vout;
        p_owner->p_vout = NULL;
        vlc_mutex_unlock( &p_owner->lock );

        p_vout = vout_Request( p_dec, p_vout, &p_dec->fmt_out.video );

        vlc_mutex_lock( &p_owner->lock );
        p_owner->p_vout = p_vout;
        vlc_mutex_unlock( &p_owner->lock );

        var_SetBool( p_owner->p_input, "intf-change-vout", true );
        if( p_vout == NULL )
        {
            msg_Err( p_dec, "failed to create video output" );
            p_dec->b_error = true;
            return NULL;
        }

        if( p_owner->video.i_rmask )
            p_owner->p_vout->render.i_rmask = p_owner->video.i_rmask;
        if( p_owner->video.i_gmask )
            p_owner->p_vout->render.i_gmask = p_owner->video.i_gmask;
        if( p_owner->video.i_bmask )
            p_owner->p_vout->render.i_bmask = p_owner->video.i_bmask;
    }

    /* Get a new picture
     */
    for( ;; )
    {
        picture_t *p_picture;

        if( p_dec->b_die || p_dec->b_error )
            return NULL;

        /* The video filter chain required that there is always 1 free buffer
         * that it will use as temporary one. It will release the temporary
         * buffer once its work is done, so this check is safe even if we don't
         * lock around both count() and create().
         */
        if( vout_CountPictureAvailable( p_owner->p_vout ) >= 2 )
        {
            p_picture = vout_CreatePicture( p_owner->p_vout, 0, 0, 0 );
            if( p_picture )
                return p_picture;
        }

        if( DecoderIsFlushing( p_dec ) )
            return NULL;

        /* */
        DecoderSignalBuffering( p_dec, true );

        /* Check the decoder doesn't leak pictures */
        vout_FixLeaks( p_owner->p_vout, false );

        msleep( VOUT_OUTMEM_SLEEP );
    }
}

static void vout_del_buffer( decoder_t *p_dec, picture_t *p_pic )
{
    vout_DropPicture( p_dec->p_owner->p_vout, p_pic );
}

static void vout_link_picture( decoder_t *p_dec, picture_t *p_pic )
{
    vout_LinkPicture( p_dec->p_owner->p_vout, p_pic );
}

static void vout_unlink_picture( decoder_t *p_dec, picture_t *p_pic )
{
    vout_UnlinkPicture( p_dec->p_owner->p_vout, p_pic );
}

static subpicture_t *spu_new_buffer( decoder_t *p_dec )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    vout_thread_t *p_vout = NULL;
    subpicture_t *p_subpic;
    int i_attempts = 30;

    while( i_attempts-- )
    {
        if( p_dec->b_die || p_dec->b_error )
            break;

        p_vout = vlc_object_find( p_dec, VLC_OBJECT_VOUT, FIND_ANYWHERE );
        if( p_vout )
            break;

        msleep( VOUT_DISPLAY_DELAY );
    }

    if( !p_vout )
    {
        msg_Warn( p_dec, "no vout found, dropping subpicture" );
        return NULL;
    }

    if( p_owner->p_spu_vout != p_vout )
    {
        vlc_mutex_lock( &p_owner->lock );

        DecoderFlushBuffering( p_dec );

        vlc_mutex_unlock( &p_owner->lock );

        spu_Control( p_vout->p_spu, SPU_CHANNEL_REGISTER,
                     &p_owner->i_spu_channel );
        p_owner->i_spu_order = 0;
        p_owner->p_spu_vout = p_vout;
    }

    p_subpic = subpicture_New();
    if( p_subpic )
    {
        p_subpic->i_channel = p_owner->i_spu_channel;
        p_subpic->i_order = p_owner->i_spu_order++;
        p_subpic->b_subtitle = true;
    }

    vlc_object_release( p_vout );

    return p_subpic;
}

static void spu_del_buffer( decoder_t *p_dec, subpicture_t *p_subpic )
{
    decoder_owner_sys_t *p_owner = p_dec->p_owner;
    vout_thread_t *p_vout = NULL;

    p_vout = vlc_object_find( p_dec, VLC_OBJECT_VOUT, FIND_ANYWHERE );
    if( !p_vout || p_owner->p_spu_vout != p_vout )
    {
        if( p_vout )
            vlc_object_release( p_vout );
        msg_Warn( p_dec, "no vout found, leaking subpicture" );
        return;
    }

    subpicture_Delete( p_subpic );

    vlc_object_release( p_vout );
}

